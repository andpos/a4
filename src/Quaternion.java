import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Quaternions. Basic operations. */
public class Quaternion implements Cloneable {

   private static final Pattern stringPattern = Pattern.compile(
                   "([-+]?[0-9]+(?:[.][0-9]*)?(?:e[-+]?[0-9]+)?)" +
                   "([-+]?[0-9]+(?:[.][0-9]*)?(?:e[-+]?[0-9]+)?)i" +
                   "([-+]?[0-9]+(?:[.][0-9]*)?(?:e[-+]?[0-9]+)?)j" +
                   "([-+]?[0-9]+(?:[.][0-9]*)?(?:e[-+]?[0-9]+)?)k");
   private static final double epsilon = 1E-9;

   private double x, y , z , w;

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      w = a;
      x = b;
      y = c;
      z = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return w;
   }

   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart() {
      return x;
   }

   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() {
      return y;
   }

   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() {
      return z;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      // %+g format specifier will automatically prepend either + or - sign,
      // following format string does not explicitly set + signs, so dual sign +- can't happen
      return String.format(Locale.getDefault(), "%g%+gi%+gj%+gk", w, x, y, z);
      // System.out.println(new Quaternion(-1, -2, -3, 4));
      // will output: "-1.000000-2.000000i-3.000000j+4.000000k"
   }

   /** Conversion from the string to the quaternion. 
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent 
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf(String s) {
      Matcher matcher = stringPattern.matcher(s);
      if (matcher.find()) {
         return new Quaternion(
                 Double.parseDouble(matcher.group(1)),
                 Double.parseDouble(matcher.group(2)),
                 Double.parseDouble(matcher.group(3)),
                 Double.parseDouble(matcher.group(4)));
      }
      throw new IllegalArgumentException(String.format("Unable to parse quaternion from string \"%s\"", s));
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      Quaternion clone = (Quaternion)super.clone();
      clone.w = w;
      clone.x = x;
      clone.y = y;
      clone.z = z;
      return clone;
   }

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      return Math.abs(w) < epsilon &&
              Math.abs(x) < epsilon &&
              Math.abs(y) < epsilon &&
              Math.abs(z) < epsilon;
   }

   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      return new Quaternion(w, -x, -y, -z);
   }

   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
      return new Quaternion(-w, -x, -y, -z);
   }

   /** Sum of quaternions. Expressed by the formula 
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus(Quaternion q) {
      return new Quaternion(w + q.w, x + q.x, y + q.y, z + q.z);
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
      return new Quaternion(
              -x * q.x - y * q.y - z * q.z + w * q.w,
               x * q.w + y * q.z - z * q.y + w * q.x,
              -x * q.z + y * q.w + z * q.x + w * q.y,
               x * q.y - y * q.x + z * q.w + w * q.z);
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
      return new Quaternion(w * r, x * r, y * r, z * r);
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
      if (isZero()) {
         throw new RuntimeException("Division by zero, because quaternion is zero");
      }
      double r = w * w + x * x + y * y + z * z;
      return new Quaternion(w / r, -x / r, -y / r, -z / r);
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus(Quaternion q) {
      return new Quaternion(w - q.w, x - q.x, y - q.y, z - q.z);
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight(Quaternion q) {
      if (q.isZero()) {
         throw new RuntimeException("Division by zero, because quaternion q is zero");
      }
      return times(q.inverse());
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft(Quaternion q) {
      if (q.isZero()) {
         throw new RuntimeException("Division by zero, because quaternion q is zero");
      }
      return q.inverse().times(this);
   }

   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param o second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object o) {
      if (o == this) {
         return true;
      }
      if (o instanceof Quaternion) {
         Quaternion q = (Quaternion)o;
         return Math.abs(w - q.w) < epsilon &&
                 Math.abs(x - q.x) < epsilon &&
                 Math.abs(y - q.y) < epsilon &&
                 Math.abs(z - q.z) < epsilon;
      }
      return false;
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult(Quaternion q) {
      return times(q.conjugate()).plus(q.times(conjugate())).times(0.5);
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      int result = 17;
      result = 31 * result + Double.hashCode(w);
      result = 31 * result + Double.hashCode(x);
      result = 31 * result + Double.hashCode(y);
      result = 31 * result + Double.hashCode(z);
      return result;
   }

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(w * w + x * x + y * y + z * z);
   }

   /** Main method for testing purposes. 
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
      System.out.println(Quaternion.valueOf("-1-2i-3j-4k"));
      System.out.println(Quaternion.valueOf("2.0e+13-4.0e-11i-5.e-12j+6e-307k"));

      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: " 
         + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));
   }
}
// end of file
